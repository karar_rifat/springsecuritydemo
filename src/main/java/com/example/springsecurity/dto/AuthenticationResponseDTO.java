package com.example.springsecurity.dto;

import lombok.Data;

@Data
public class AuthenticationResponseDTO {
    private final String jwt;
}
