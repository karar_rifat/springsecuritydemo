package com.example.springsecurity.dto;

import lombok.Data;

@Data
public class AuthenticationRequestDTO {
    String username;
    String password;

    public AuthenticationRequestDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
