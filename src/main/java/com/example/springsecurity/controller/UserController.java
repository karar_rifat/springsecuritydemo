package com.example.springsecurity.controller;

import com.example.springsecurity.dto.AuthenticationRequestDTO;
import com.example.springsecurity.dto.AuthenticationResponseDTO;
import com.example.springsecurity.dto.UserRequestDTO;
import com.example.springsecurity.dto.UserResponseDTO;
import com.example.springsecurity.entity.User;
import com.example.springsecurity.service.UserService;
import com.example.springsecurity.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class UserController {

    private final UserService userService;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<UserResponseDTO> getUsers(){
        return userService.getUsers();
    }

    @PostMapping("register")
    public void save(@RequestBody UserRequestDTO userRequestDTO){
        userService.save(userRequestDTO);
    }

    @PostMapping("authenticate")
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody AuthenticationRequestDTO authenticationRequestDTO
            ) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequestDTO.getUsername(), authenticationRequestDTO.getPassword())
            );
        } catch (BadCredentialsException e){
            throw new Exception("incorrect username and password");
        }
        final UserDetails userDetails =
                userDetailsService.loadUserByUsername(authenticationRequestDTO.getUsername());

        final String jwt = jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponseDTO(jwt));
    }
}
