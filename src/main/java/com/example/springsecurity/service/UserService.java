package com.example.springsecurity.service;

import com.example.springsecurity.dto.UserRequestDTO;
import com.example.springsecurity.dto.UserResponseDTO;

import java.util.List;

public interface UserService  {
    List<UserResponseDTO> getUsers();
    void save(UserRequestDTO userRequestDTO);
}
