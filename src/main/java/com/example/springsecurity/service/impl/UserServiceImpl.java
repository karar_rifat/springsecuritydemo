package com.example.springsecurity.service.impl;

import com.example.springsecurity.dto.UserRequestDTO;
import com.example.springsecurity.dto.UserResponseDTO;
import com.example.springsecurity.entity.User;
import com.example.springsecurity.mapper.User2UserResponseMapper;
import com.example.springsecurity.repo.UserRepository;
import com.example.springsecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    User2UserResponseMapper mapper;

    @Override
    public List<UserResponseDTO> getUsers() {
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        return userRepository.findAll().stream()
                .map(m -> mapper.from(m))
                .collect(Collectors.toList());

    }

    @Override
    public void save(UserRequestDTO userRequestDTO) {
        User user = new User();
        user.setEmail(userRequestDTO.getEmail());
        user.setUsername(userRequestDTO.getUsername());
        user.setPassword(userRequestDTO.getPassword());
        userRepository.save(user);
    }
}
