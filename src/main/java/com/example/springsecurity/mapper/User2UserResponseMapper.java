package com.example.springsecurity.mapper;

import com.example.springsecurity.dto.UserResponseDTO;
import com.example.springsecurity.entity.User;
import org.springframework.stereotype.Component;

@Component
public class User2UserResponseMapper {

    public UserResponseDTO from(User user){
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(user.getId());
        userResponseDTO.setUsername(user.getUsername());
        userResponseDTO.setEmail(user.getEmail());
        return userResponseDTO;
    }
}
